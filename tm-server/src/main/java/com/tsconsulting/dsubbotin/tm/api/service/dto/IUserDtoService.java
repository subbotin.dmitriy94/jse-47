package com.tsconsulting.dsubbotin.tm.api.service.dto;

import com.tsconsulting.dsubbotin.tm.dto.UserDTO;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

public interface IUserDtoService extends IDtoService<UserDTO> {

    void removeByIndex(int index) throws AbstractException;

    UserDTO create(
            @NotNull String login,
            @NotNull String password
    ) throws AbstractException;

    UserDTO create(
            @NotNull String login,
            @NotNull String password,
            @NotNull Role role
    ) throws AbstractException;

    UserDTO create(
            @NotNull String login,
            @NotNull String password,
            @NotNull Role role,
            @NotNull String email
    ) throws AbstractException;

    void clear(@NotNull String userId) throws AbstractException;

    @NotNull
    UserDTO findByLogin(@NotNull String login) throws AbstractException;

    void removeByLogin(@NotNull String login) throws AbstractException;

    void removeById(@NotNull String id) throws AbstractException;

    void setPassword(@NotNull String id, @NotNull String password) throws AbstractException;

    void setRole(@NotNull String id, @NotNull Role role) throws AbstractException;

    void updateById(
            @NotNull String id,
            @NotNull String lastName,
            @NotNull String firstName,
            @NotNull String middleName,
            @NotNull String email
    ) throws AbstractException;

    boolean isLogin(@NotNull String login) throws AbstractException;

    void lockByLogin(@NotNull String login) throws AbstractException;

    void unlockByLogin(@NotNull String login) throws AbstractException;

}
