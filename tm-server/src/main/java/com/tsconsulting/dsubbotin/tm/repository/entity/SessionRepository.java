package com.tsconsulting.dsubbotin.tm.repository.entity;

import com.tsconsulting.dsubbotin.tm.api.repository.entity.ISessionRepository;
import com.tsconsulting.dsubbotin.tm.entity.Session;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.SessionNotFoundException;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import java.util.List;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void open(@NotNull final Session session) {
        entityManager.persist(session);
    }

    @Override
    public void close(@NotNull final Session session) {
        entityManager.remove(session);
    }

    @Override
    public boolean contains(@NotNull final Session session) {
        return entityManager
                .createQuery("FROM Session WHERE id = :id", Session.class)
                .setParameter("id", session.getId())
                .getResultStream()
                .findFirst()
                .isPresent();
    }

    @Override
    public boolean existByUserId(@NotNull final String userId) {
        return entityManager
                .createQuery("FROM Session WHERE user.id = :userId", Session.class)
                .setParameter("userId", userId)
                .getResultStream()
                .findFirst()
                .isPresent();
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        return entityManager.createQuery("FROM Session", Session.class).getResultList();
    }

    @NotNull
    @Override
    public List<Session> findAll(@NotNull final String userId) {
        return entityManager
                .createQuery("FROM Session WHERE user.id = :userId", Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public Session findById(@NotNull final String id) throws AbstractException {
        return entityManager
                .createQuery("FROM Session WHERE id = :id", Session.class)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .orElseThrow(SessionNotFoundException::new);
    }

    @NotNull
    @Override
    public Session findByIndex(final int index) throws AbstractException {
        return entityManager
                .createQuery("FROM Session", Session.class)
                .setMaxResults(1)
                .setFirstResult(index)
                .getResultStream()
                .findFirst()
                .orElseThrow(SessionNotFoundException::new);
    }

    @Override
    public void clear() {
        findAll().forEach(entityManager::remove);
    }

    @Override
    public void clear(@NotNull final String userId) {
        findAll(userId).forEach(entityManager::remove);
    }

    @Override
    public boolean existById(@NotNull final String id) {
        return entityManager
                .createQuery("FROM Session WHERE id = :id", Session.class)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .isPresent();
    }

}
