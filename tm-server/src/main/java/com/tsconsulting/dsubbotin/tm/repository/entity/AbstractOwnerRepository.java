package com.tsconsulting.dsubbotin.tm.repository.entity;

import com.tsconsulting.dsubbotin.tm.api.repository.entity.IOwnerRepository;
import com.tsconsulting.dsubbotin.tm.entity.AbstractOwnerEntity;
import com.tsconsulting.dsubbotin.tm.entity.User;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IOwnerRepository<E> {

    public AbstractOwnerRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void create(@NotNull final String userId, @NotNull final E entity) {
        @NotNull final User user = entityManager
                .createQuery("FROM User WHERE id = :id", User.class)
                .setParameter("id", userId)
                .getSingleResult();
        entity.setUser(user);
        entityManager.persist(entity);
    }

}
