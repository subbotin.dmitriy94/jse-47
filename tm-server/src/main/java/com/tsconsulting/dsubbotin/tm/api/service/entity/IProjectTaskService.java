package com.tsconsulting.dsubbotin.tm.api.service.entity;

import com.tsconsulting.dsubbotin.tm.entity.Task;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IProjectTaskService {

    void bindTaskToProject(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    ) throws AbstractException;

    void unbindTaskFromProject(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    ) throws AbstractException;

    @NotNull
    List<Task> findAllTasksByProjectId(@NotNull String userId, @NotNull String id) throws AbstractException;

    void removeProjectById(@NotNull String userId, @NotNull String id) throws AbstractException;

    void removeProjectByIndex(@NotNull String userId, int index) throws AbstractException;

    void removeProjectByName(@NotNull String userId, @NotNull String name) throws AbstractException;

    void removeAllProject(@NotNull String userId) throws AbstractException;

}
