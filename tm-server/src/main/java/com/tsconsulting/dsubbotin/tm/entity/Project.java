package com.tsconsulting.dsubbotin.tm.entity;

import com.tsconsulting.dsubbotin.tm.api.entity.IWBS;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "projects")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Project extends AbstractOwnerEntity implements IWBS {

    @Column
    @NotNull
    private String name;

    @Column
    @Nullable
    private String description;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "start_date")
    private Date startDate = null;

    @NotNull
    @Column(name = "create_date")
    private Date createDate = new Date();

    @Nullable
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @Override
    public String toString() {
        return super.toString() + name + "; " +
                "Status: " + status + "; " +
                "Started: " + startDate + "; " +
                "Created: " + createDate + ";";
    }

}