package com.tsconsulting.dsubbotin.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractOwnerEntity extends AbstractEntity {

    @NotNull
    @ManyToOne
    private User user;

    @NotNull
    @Override
    public String toString() {
        return super.toString() + " - User Id: " + user.getId() + "; ";
    }

}