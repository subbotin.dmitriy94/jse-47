<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <parent>
        <artifactId>task-manager</artifactId>
        <groupId>com.tsconsulting.dsubbotin.tm</groupId>
        <version>1.42.1</version>
    </parent>

    <modelVersion>4.0.0</modelVersion>
    <artifactId>tm-server</artifactId>
    <version>0.1.2</version>

    <name>task-manager-server</name>
    <url>http://www.tsconsulting.com/</url>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>15</maven.compiler.source>
        <maven.compiler.target>15</maven.compiler.target>
    </properties>

    <developers>

        <developer>
            <id>dsubbotin</id>
            <name>Dmitriy Subbotin</name>
            <email>dsubbotin@tsconsulting.com</email>
            <organization>TechnoServConsulting</organization>
            <url>http://www.tsconsulting.com/</url>
        </developer>

    </developers>

    <scm>
        <connection>scm:svn:http://none</connection>
        <developerConnection>scm:svn:https://none</developerConnection>
        <url>http://none</url>
    </scm>

    <dependencies>

        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.20</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.jetbrains</groupId>
            <artifactId>annotations</artifactId>
            <version>20.1.0</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.jcabi</groupId>
            <artifactId>jcabi-manifests</artifactId>
            <version>1.1</version>
            <scope>compile</scope>
        </dependency>

        <dependency>
            <groupId>com.fasterxml.jackson.dataformat</groupId>
            <artifactId>jackson-dataformat-xml</artifactId>
            <version>2.12.3</version>
        </dependency>

        <dependency>
            <groupId>org.eclipse.persistence</groupId>
            <artifactId>org.eclipse.persistence.moxy</artifactId>
            <version>2.7.2</version>
        </dependency>

        <dependency>
            <groupId>com.sun.xml.ws</groupId>
            <artifactId>rt</artifactId>
            <version>2.3.1</version>
        </dependency>

        <dependency>
            <groupId>javax.xml.ws</groupId>
            <artifactId>jaxws-api</artifactId>
            <version>2.3.1</version>
            <type>pom</type>
        </dependency>

        <dependency>
            <groupId>org.postgresql</groupId>
            <artifactId>postgresql</artifactId>
            <version>42.2.21</version>
        </dependency>

        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <version>1.7.30</version>
        </dependency>

        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-core</artifactId>
            <version>5.5.3.Final</version>
        </dependency>

        <dependency>
            <groupId>com.hazelcast</groupId>
            <artifactId>hazelcast-all</artifactId>
            <version>4.2.1</version>
        </dependency>

        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-c3p0</artifactId>
            <version>5.5.6.Final</version>
        </dependency>

        <dependency>
            <groupId>org.hsqldb</groupId>
            <artifactId>hsqldb</artifactId>
            <version>2.6.0</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>test</scope>
        </dependency>

    </dependencies>

    <build>

        <finalName>task-manager-server</finalName>

        <plugins>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <version>3.2.0</version>
                <configuration>
                    <archive>
                        <manifest>
                            <addClasspath>true</addClasspath>
                            <mainClass>com.tsconsulting.dsubbotin.tm.Application</mainClass>
                        </manifest>
                        <manifestEntries>
                            <buildNumber>${buildNumber}</buildNumber>
                            <version>${project.version}</version>
                            <buildVersion>${project.version}-RELEASE.${buildNumber}</buildVersion>
                            <timestamp>${timestamp}</timestamp>
                            <artifactId>${project.artifactId}</artifactId>
                            <groupId>${project.groupId}</groupId>
                            <developer>Dmitriy Subbotin</developer>
                            <email>dsubbotin@tsconsulting.com</email>
                        </manifestEntries>
                    </archive>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-shade-plugin</artifactId>
                <version>3.2.4</version>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                        <configuration>
                            <transformers>
                                <transformer
                                        implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                                    <mainClass>com.tsconsulting.dsubbotin.tm.Application</mainClass>
                                </transformer>
                            </transformers>
                            <filters>
                                <filter>
                                    <artifact>*:*</artifact>
                                    <excludes>
                                        <exclude>META-INF/*.SF</exclude>
                                        <exclude>META-INF/*.DSA</exclude>
                                        <exclude>META-INF/*.RSA</exclude>
                                    </excludes>
                                </filter>
                            </filters>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>buildnumber-maven-plugin</artifactId>
                <version>1.4</version>
                <executions>
                    <execution>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>create</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <revisionOnScmFailure>no.scm.config.in.pom</revisionOnScmFailure>
                    <doCheck>true</doCheck>
                    <doUpdate>false</doUpdate>
                    <format>{0,number}</format>
                    <items>
                        <item>buildNumber</item>
                        <item>timestamp</item>
                    </items>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.liquibase</groupId>
                <artifactId>liquibase-maven-plugin</artifactId>
                <version>4.4.3</version>
                <configuration>
                    <propertyFile>liquibase.properties</propertyFile>
                </configuration>
                <dependencies>
                    <dependency>
                        <groupId>org.postgresql</groupId>
                        <artifactId>postgresql</artifactId>
                        <version>42.2.21</version>
                    </dependency>
                </dependencies>
            </plugin>

        </plugins>

    </build>

</project>