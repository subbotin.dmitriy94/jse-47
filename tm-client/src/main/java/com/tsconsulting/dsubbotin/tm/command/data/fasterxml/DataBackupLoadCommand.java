package com.tsconsulting.dsubbotin.tm.command.data.fasterxml;

import com.tsconsulting.dsubbotin.tm.command.AbstractCommand;
import com.tsconsulting.dsubbotin.tm.component.Backup;
import com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DataBackupLoadCommand extends AbstractCommand {

    @Override
    public @NotNull String name() {
        return Backup.getLoadCommand();
    }

    @Override
    public @NotNull String description() {
        return "Load backup data.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable SessionDTO session = endpointLocator.getSessionService().getSession();
        endpointLocator.getAdminEndpoint().loadBackup(session);
    }

}
