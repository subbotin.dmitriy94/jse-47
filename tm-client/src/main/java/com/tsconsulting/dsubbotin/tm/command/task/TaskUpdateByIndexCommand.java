package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-update-by-index";
    }

    @Override
    @NotNull
    public String description() {
        return "Update task by index.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable SessionDTO session = endpointLocator.getSessionService().getSession();
        TerminalUtil.printMessage("Enter index:");
        final int index = TerminalUtil.nextNumber();
        endpointLocator.getTaskEndpoint().findByIndexTask(session, index);
        TerminalUtil.printMessage("Enter name:");
        @NotNull final String name = TerminalUtil.nextLine();
        TerminalUtil.printMessage("Enter description:");
        @NotNull final String description = TerminalUtil.nextLine();
        endpointLocator.getTaskEndpoint().updateByIndexTask(session, index, name, description);
        TerminalUtil.printMessage("[Task updated]");
    }

}
