package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-update-by-index";
    }

    @Override
    @NotNull
    public String description() {
        return "Update project by index.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable SessionDTO session = endpointLocator.getSessionService().getSession();
        TerminalUtil.printMessage("Enter index:");
        final int index = TerminalUtil.nextNumber();
        endpointLocator.getProjectEndpoint().findByIndexProject(session, index);
        TerminalUtil.printMessage("Enter name:");
        @NotNull final String name = TerminalUtil.nextLine();
        TerminalUtil.printMessage("Enter description:");
        @NotNull final String description = TerminalUtil.nextLine();
        endpointLocator.getProjectEndpoint().updateByIndexProject(session, index, name, description);
        TerminalUtil.printMessage("[Project updated]");
    }

}
