package com.tsconsulting.dsubbotin.tm.util;

import com.tsconsulting.dsubbotin.tm.endpoint.Role;
import com.tsconsulting.dsubbotin.tm.endpoint.Status;
import com.tsconsulting.dsubbotin.tm.enumerated.Sort;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownRoleException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownSortException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownStatusException;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;

@UtilityClass
public final class EnumerationUtil {

    @NotNull
    public static Status parseStatus(@Nullable final String name) throws UnknownStatusException {
        return Arrays
                .stream(Status.values())
                .filter(status -> status.name().equals(name))
                .findFirst().orElseThrow(UnknownStatusException::new);
    }

    @NotNull
    public static Sort parseSort(@Nullable final String name) throws UnknownSortException {
        return Arrays
                .stream(Sort.values())
                .filter(sort -> sort.name().equals(name))
                .findFirst().orElseThrow(UnknownSortException::new);
    }

    @NotNull
    public static Role parseRole(@Nullable final String name) throws UnknownRoleException {
        return Arrays
                .stream(Role.values())
                .filter(role -> role.name().equals(name))
                .findFirst().orElseThrow(UnknownRoleException::new);
    }

}
