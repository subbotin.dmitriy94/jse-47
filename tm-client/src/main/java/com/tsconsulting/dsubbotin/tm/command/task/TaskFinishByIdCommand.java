package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskFinishByIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-finish-by-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Finish task by id.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable SessionDTO session = endpointLocator.getSessionService().getSession();
        TerminalUtil.printMessage("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        endpointLocator.getTaskEndpoint().finishByIdTask(session, id);
        TerminalUtil.printMessage("[Updated task status]");
    }

}
